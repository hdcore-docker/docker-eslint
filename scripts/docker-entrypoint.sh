#!/bin/sh

# Author: Danny Herpol <hdcore@lachjekrom.com>
# Version GIT: 2025-02-15 10:38

# docker-entrypoint.sh
# for hdcore docker-eslint image

CGREEN="\e[32m"
CNORMAL="\e[0m"

# shellcheck disable=SC2059
printf "== ${CGREEN}Start entrypoint ${0}${CNORMAL} ==\n"

printf "HDCore docker-eslint container image\n"
printf "Image creation date:\n"
date -r /tools +'%F %T %z'
printf "Installed version of eslint:\n"
eslint --version

# shellcheck disable=SC2059
printf "== ${CGREEN}End entrypoint ${0}${CNORMAL} ==\n"

# Execute docker CMD
exec "$@"
