#!/bin/sh

# Author: Danny Herpol <hdcore@lachjekrom.com>
# Version GIT: 2021-11-13 17:21

# docker-entrypoint.sh
# for hdcore docker-eslint image

if [ -z "${1}" ]
then
    echo "Error: no parameter given!"
    exit 99
fi

# shellcheck disable=SC2068 # double quote not allowed due to extra parameter expansion
eslint --rule 'import/no-unresolved:0' $@
