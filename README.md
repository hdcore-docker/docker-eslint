# HDCore - docker-eslint

## Introduction

This is a small container image that contains a Node environment with eslint. This container is very usefull for automatic testing during CI.

## Image usage

- Run eslint:

```bash
docker run --rm -v /path/to/workspace:/workspace hdcore/docker-eslint:<version> eslint.sh <arguments>
```

- Run shell:

```bash
docker run -it --rm -v /path/to/workspace:/workspace hdcore/docker-eslint:<version> /bin/sh
```

- Use in .gitlab-ci.yml:

```bash
image: hdcore/docker-eslint:<version>
script: eslint.sh <arguments>
```

## Available tags

- hdcore/docker-eslint:9

## Container Registries

The image is stored on multiple container registries at dockerhub and gitlab:

- Docker Hub:
  - hdcore/docker-eslint
  - registry.hub.docker.com/hdcore/docker-eslint
- Gitlab:
  - registry.gitlab.com/hdcore-docker/docker-eslint

## Building image

Build:

```bash
docker build -f <version>/Dockerfile -t docker-eslint:<version> .
```
